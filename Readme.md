#Description
This image will download the latest artifact from maven repository (only tested with Artifactory) and run it in a tomcat.
If you fill PGHOST env variable it will make a postgres check connection, and wait to start tomcat until database connection is available

#Usages:
* REPO_URL=http://my-artifactory.com/artifactory
* REPO_USERNAME=username
* REPO_PASSWORD=password
* REPO_NAME=libs-snapshot-local
* ARTIFACT_GROUP=com.enterprise
* ARTIFACT_ID=name
* ARTIFACT_VERSION=2.2.6-SNAPSHOT
* PGHOST=postgres_host_to_check_availability (optional)
* PGPORT=postgres_port_to_check_availability (optional)
* PGUSER=postgres_username_to_check_availability (optional)
* PGPASSWORD=postgres_password_to_check_availability (optional)