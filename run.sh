#!/bin/bash

set -e

echo 'Initializing '
path=$REPO_URL/$REPO_NAME/${ARTIFACT_GROUP//[.]/\/}/$ARTIFACT_ID
build=`curl -u $REPO_USERNAME:$REPO_PASSWORD -s $path/$ARTIFACT_VERSION/maven-metadata.xml | grep '<value>' | head -1 | sed "s/.*<value>\([^<]*\)<\/value>.*/\1/"`

#if there is no value for build it's because it's a release so use the artifact version for download
if [ -z "$build" ]
then
    build=$ARTIFACT_VERSION
fi

war=$ARTIFACT_ID-$build.war
url=$path/$ARTIFACT_VERSION/$war

echo 'Downloading artifact at' ${url}
wget --progress=dot:mega --user=$REPO_USERNAME --password=$REPO_PASSWORD -P /tmp/ -N $url

echo 'Installing application'
rm -rf /usr/local/tomcat/webapps/ROOT*
cp /tmp/*.war /usr/local/tomcat/webapps/ROOT.war

echo 'Checking if we need to wait for a postgresql database to accept connections before starting'
if [ -z "$PGHOST" ]; then
    echo 'No PG_HOST specified skipping postgres database connection check'
else
    until psql -c '\l';
    do
      >&2 echo "Postgres is unavailable - sleeping"
      sleep 5
    done
      >&2 echo "Postgres is up - launching tomcat"
fi

catalina.sh run